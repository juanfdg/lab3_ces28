package models;

public interface PainelCondutor {
    boolean imprimirAviso(String msg, int i);

    void diminuiVelocidadeTrem(double valor);

    void aceleraVelocidadeTrem(double valor);
}

package models;

public interface Sensor {
    double getVelocidade();

    boolean isCruzamento();
}

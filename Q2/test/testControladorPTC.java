import models.ControladorPTC;
import models.Datacenter;
import models.PainelCondutor;
import models.Sensor;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class testControladorPTC {
    private Sensor sensor = mock(Sensor.class);
    private Datacenter dataCenter = mock(Datacenter.class);
    private PainelCondutor painelCond = mock(PainelCondutor.class);
    private ControladorPTC ptc;

    @Before
    public void setUp() {
        ptc = new ControladorPTC(sensor, dataCenter, painelCond);
    }

    @Test
    public void testSemCruzamento() {
        when(sensor.getVelocidade()).thenReturn((double) 50);
        when(sensor.isCruzamento()).thenReturn(false);

        ptc.run();

        verify(sensor).getVelocidade();
        verify(sensor).isCruzamento();
        verify(dataCenter).gerarRelatorio();
        verify(painelCond).imprimirAviso(String.valueOf((double) 50), 1);
    }

    @Test
    public void testCruzamentoRapido() {
        when(sensor.getVelocidade()).thenReturn((double) 200);
        when(sensor.isCruzamento()).thenReturn(true);
        when(painelCond.imprimirAviso(anyString(), anyInt())).thenReturn(true);

        ptc.run();

        verify(sensor).getVelocidade();
        verify(sensor).isCruzamento();
        verifyNoMoreInteractions(sensor);

        verify(painelCond).imprimirAviso(anyString(), anyInt());
        verifyNoMoreInteractions(painelCond);

        verifyZeroInteractions(dataCenter);
    }

    @Test
    public void testCruzamentoDevagar() {
        when(sensor.getVelocidade()).thenReturn((double) 10);
        when(sensor.isCruzamento()).thenReturn(true);
        when(painelCond.imprimirAviso(anyString(), anyInt())).thenReturn(false);

        ptc.run();

        verify(sensor).getVelocidade();
        verify(sensor).isCruzamento();
        verifyNoMoreInteractions(sensor);

        verify(painelCond, times(2)).imprimirAviso(anyString(), anyInt());
        verify(painelCond).aceleraVelocidadeTrem((double) 20);
        verifyNoMoreInteractions(painelCond);

        verifyZeroInteractions(dataCenter);
    }
}
